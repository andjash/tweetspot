//
//  SettingsService.swift
//  TweetSpot
//
//  Created by Andrey Yashnev on 14/08/16.
//  Copyright © 2016 Andrey Yashnev. All rights reserved.
//

import Foundation

@objc protocol SettignsService {    
    var shouldDisplayUserAvatarsOnSpot: Bool { get set }
}