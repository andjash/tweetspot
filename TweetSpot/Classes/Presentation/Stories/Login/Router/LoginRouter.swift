//
//  LoginLoginRouter.swift
//  TweetSpot
//
//  Created by Andrey Yashnev on 12/08/2016.
//  Copyright © 2016 Andrey Yashnev. All rights reserved.
//

import Foundation
import ViperMcFlurry

class LoginRouter: NSObject, LoginRouterInput {

	weak var transitionHandler: RamblerViperModuleTransitionHandlerProtocol!

    func closeModule() {
        transitionHandler.closeCurrentModule?(true)
    }
}
